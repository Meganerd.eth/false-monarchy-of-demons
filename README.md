# `False Monarchy of Demons`

## Project Information
* Language: Python 2.7
* GUI Library: Pyglet + Rabbyt
* License: The game code will be released with a FOSS compatible license on 1.0 release

## Dev Notes
```
Current progress: In development
No release candidates at this moment. Currently I am wrapping up the core game engine.
```

## Game Features / Tags
* Item crafting (wood cut, fletch, mine, smelt, craft, +more)
* Base building
* Open world
* Turn-based / Tick-based hybrid system (Think OSRS but instead of PID order will be by agility high->low)
* Colony simulation (Think Dwarf Fortress or Rimworld)
* Fantasy theme
* Magic
* RPG (Skill-tree, character leveling, etc)


##### Dev Update \#1 9.7.2020
- [x] Initialize public repository for FMoD
- [x] Demo webm of misc things
![Demo_004](../Demo/Demo_0x04_Rain.webm)
![Demo_005](../Demo/Demo_0x05_Proof_of_concept_Z-levels.webm)
![Demo_006](../Demo/Demo_0x06_Zoom_Testing.webm)
![Demo_007](../Demo/Demo_0x07_Test_Attack_Animation.webm)

## Game Credits
* Music: Eric Matyas at https://soundimage.org/fantasywonder/ (Royalty free; Requires attribution)
* NPC & Item Artwork: Oryx Design Lab at oryx@oryxdesignlab.com https://oryxdesignlab.com/license (Purchased)
* Item Artwork: finalbossblues at https://finalbossblues.com https://timefantasy.net (Purchased)
* Animation Artwork: Pixel Effects by Henry Software at https://henrysoftware.itch.io/pixel-effects (Purchased)


## Changelog
- [x] A* pathfinding implemented 8.7.2020
- [x] Created fancy menu & button backdrops 8.10.2020
- [x] Add fonts 8.13.2020
- [x] Add poison animation 8.13.2020
- [x] Add music player class 8.14.2020
- [x] Created game time class 8.20.2020
- [x] Created night time shade 8.20.2020
- [x] Turn speed multiplyer 2x 4x 8.28.2020
- [x] Created Weather class 8.30.2020
- [x] Created rain animation 8.30.2020
- [x] Add beer and potion items 8.30.2020
- [x] Improved misc tile sprites 9.2.2020
- [x] Add explosion animation 9.2.2020
- [x] Created more grimoire sprites 9.2.2020
- [x] Created bed sprite 9.2.2020
- [x] Created anvil sprite 9.2.2020
- [x] Created furnace sprite 9.2.2020
- [x] Created iron fence sprite 9.3.2020
- [x] Fix sprite scaling using OpenGL 9.5.2020
- [x] Add map zoom 2x-4x 9.6.2020
- [x] Add map scrolling 9.6.2020
- [x] Initialize public repository for FMoD 9.7.2020
- [x] Add misc webm demo 9.7.2020
- [x] Add air strike attack 9.7.2020
- [x] Add air barrage attack 9.7.2020
- [x] Add inventory activespell highlighting 9.8.2020
- [x] Created red, yellow, blue, green, black, orange, white, purple isometric cubic effect animations 9.8.2020
- [x] Add effect animation special attribute and clock to objects 9.9.2020
- [x] Created electricity, furnace smoke animations 9.9.2020
- [x] Created wooden sign, tree cut sprites 9.9.2020
- [x] Added thunder storm weather effect (WIP) 9.9.2020
- [x] Created alert class and alert manager 9.19.2020
- [x] Created stamina bars 9.22.2020
- [x] Fixed selected tab highlighting 9.22.2020
- [x] Battle mode game processor started 9.23.2020
- [x] Changed animation wait conditions to list instead of single object 9.23.2020
- [x] Volume increase/decrease modifer and correct defaults 9.23.2020
- [x] Volume controls added to GUI settings 9.23.2020
- [x] NPC phase I, II, III icons created 9.23.2020
- [x] NPC phase testing 9.24.2020



